#adev runserver server.py --app-factory init_app
import logging.config
from traceback import format_exc

from api import settings

logging.config.dictConfig(settings.LOG_CONFIG)
logger = logging.getLogger("main")

try:

    import os
    import sys
    import argparse

    parser = argparse.ArgumentParser(description="aiohttp server example")
    parser.add_argument('--path')

    from aiohttp import web
    import aiohttp_jinja2
    import jinja2

    from api.routes import setup_routes
    from api.middlewares import exceptions_handling_middleware, check_client_middleware #, request_response_logging, get_user_middleware


    async def init_app():
        app = web.Application(client_max_size=10*1024*1024)    
        app['config'] = settings
        setup_routes(app)
        app.middlewares.append(exceptions_handling_middleware)
        app.middlewares.append(check_client_middleware)
        #app.middlewares.append(get_user_middleware)
        #app.middlewares.append(request_response_logging)
        aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('api/templates'))
        
        #setup_middlewares(app)
        #app.on_startup.append(init_pg)
        #app.on_cleanup.append(close_pg)
        return app
        

    def main():
        app = init_app()
        args = parser.parse_args()
        web.run_app(app, path=args.path)
except Exception as e:
    logger.exception(format_exc())
    raise e


if __name__ == '__main__':
    main()