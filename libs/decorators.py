from base64 import b64encode
import logging.config
import re
from traceback import format_exc
import json

from aiohttp import web 
from passlib.hash import pbkdf2_sha256

from settings import *
from libs.db import *
from libs.redis import *
from libs.common import delete_user_tokens


logging.config.dictConfig(LOG_CONFIG)
logger = logging.getLogger("main")


def get_user_by_token_decorator(func):
    async def wrapped(*args, **kwargs):
        logger.info('Аутентификация по токену')
        request = args[0].request
        dbResp = None
        authenticated = False
        token = request.headers.get('token')
        if token is not None: #auth by token
            logger.info(f"Токен найден. Проверка валидности токена")
            async with RedisCon() as redis:
                userId = await redis.execute('get', f'{REDIS_TOKEN_PREFIX}{token}') 
                if userId is None:  #check if token must be updated
                    key = await redis.execute('get', f'{REDIS_TOKEN_UPDATE_PREFIX}{token}') 
                    if key is not None: #update token
                        return web.HTTPUnauthorized()
                else:
                    logger.info(f"Токен валиден. ID пользователя: {userId}. Поиск в БД")
                    async with DataBase() as db:
                        dbResp = await db.execute_query("""
                            SELECT id, is_active, login, phone_number
                            FROM auth_user au 
                            WHERE au.id = $1
                            """, 
                            int(userId)
                        )
                    if dbResp: #user found in DB                    
                        dbResp = dbResp[0]
                        logger.info(f"Пользователь найден. Логин: {dbResp['login']}")
                        if dbResp['is_active']: #user is active - get user
                            authenticated = True
                            logger.info(f"Пользователь {dbResp['login']} активен")
                        else:   #user is not active - delete api-token
                            logger.info(f"Пользователь {dbResp['login']} заблокирован. Удаление токена пользователя...")
                            refreshToken = await redis.execute('get', f'{REDIS_TOKEN_UPDATE_PREFIX}{token}') 
                            if  refreshToken is not None:
                                await delete_user_tokens(token, refreshToken)
                            else:
                                await redis.con.delete([token])
                            logger.info(f"Токен {token} удален")
        else:
            logger.info(f"Токен отсутствует в запросе")
        if authenticated:
            request.user = {'id': dbResp['id'], 'login': dbResp['login'], 'token': token, 'phone_number': dbResp['phone_number']}
            logger.info(f"Реквизиты пользователя: {str(request.user)}")
            return await func(*args, **kwargs)
        else:
            logger.info("Возвращаем форбидден")
            return web.HTTPForbidden()
    return wrapped


def get_user_by_logopass_decorator(func):
    async def wrapped(*args, **kwargs):
        logger.info('Аутентификация по логопассу')
        request = args[0].request
        try:
            data = await request.json()
        except json.decoder.JSONDecodeError:
            return web.HTTPForbidden()
        
        dbResp = None
        authenticated = False
        login = data.get('login')        
        if login is not None and 'password' in data:    #auth by username and pasword
            login = login.strip().lower()
            logger.info(f"Логин пользователя: {login}. Поиск в БД..")
            async with DataBase() as db:
                dbResp = await db.execute_query("""
                    SELECT id, password, is_active, login, phone_number
                    FROM auth_user au 
                    WHERE login = $1
                    """, 
                    login
                )
            if dbResp: #user found in DB
                dbResp = dbResp[0]
                logger.info(f"Пользователь {login} найден")
                if dbResp['is_active']:
                    logger.info(f"Пользователь {login} активен. Проверка пароля...")
                    #password = dbResp['password'].split('$')
                    #password[0] = password[0].replace('_', '-')
                    #print(22, password[2])
                    #password[2] = b64encode(password[2].encode('utf8')).decode('utf8')
                    #print(666, password[2])
                    #if pbkdf2_sha256.verify(data['password'], f'${"$".join(password)}'):
                    if pbkdf2_sha256.verify(data['password'], dbResp['password']):
                        authenticated = True
                        logger.info(f"Пароль корректен")
                    else:
                        logger.info(f"Пароль некорректен")
                else:
                    logger.info(f"Пользователь {login} заблокирован..")
        if authenticated:
            request.user = {'id': dbResp['id'], 'login': dbResp['login'], 'phone_number': dbResp['phone_number']}
            logger.info(f"Реквизиты пользователя: {str(request.user)}")
            return await func(*args, **kwargs)
        else:
            logger.info("Возвращаем форбидден")
            return web.HTTPForbidden()
    return wrapped



async def validate_request_params(fields, func, data, *args, **kwargs):
    """
    Validate params in an incomming request
    :fields = {
        'required': {<param_name1>: <regex1>, ...},
        'variable': {<param_name1>: <regex1>, ...},
        'or_params': ([param_name1, param_name2], ...),
        'error_messages': {<param_name1>: <Error mes>, ...}
        }
    :func - decorated function
    :data - request params
    :*args, **kwargs - decorated function arguments
    """
    request = args[0].request
    #logger.info(f"Входящий запрос на: {request.url}\nПараметры: {str(data)}")
    logger.info(f"Валидатор полей: {str(fields)}")
    errors = {}
    #required fields
    if 'required' in fields:
        for param, regex in fields['required'].items():
            print(param, data)
            if param in data:
                #print(4444, regex, str(param), re.match(regex, str(param)))
                strValue = str(data[param])
                if not re.match(regex, strValue):
                    if('error_messages' in fields and param in fields['error_messages']):
                        errors[param] = fields['error_messages'][param]
                    else:
                        errors[param] = f'doesnt match the regex: {regex}' if len(strValue) > 0 else 'is required'
            else:
                errors[param] = 'is required'
    #variable fields
    if 'variable' in fields:
        for param, regex in fields['variable'].items():
            if param in data:
                #print(4444, regex, str(param), re.match(regex, str(param)))
                if not re.match(regex, str(data[param])):
                    if('error_messages' in fields and param in fields['error_messages']):
                        errors[param] = fields['error_messages'][param]
                    else:
                        errors[param] = f'doesnt match the regex: {regex}'
    #or fields
    dataKeys = data.keys()
    for fields in fields.get('or_params', []):
        if not any(el in dataKeys for el in fields):
            fieldsStr = str(fields)
            errors[fieldsStr] = f'any param is required: {fieldsStr}'
            
    if not errors:
        logger.info('Параметры валидны. Запуск вьюхи.')
        response = await func(*args, **kwargs)
    else: 
        logger.info('Параметры невалидны.')
        response = web.json_response({'errors': errors, 'code': INCORRECT_PARAMS})
    #print(54545, response._body)
    #logger.info(f'Ответ: {response._body}')
    return response


def validate_get_params_decorator(**fields):
    """
    Validate params in an incomming request
    *fields = {
        'required': {<param_name1>: <regex1>, ...},
        'variable': {<param_name1>: <regex1>, ...},
    }
    """
    def validate_get_params(func):  
        async def wrapped(*args, **kwargs):
            return await validate_request_params(fields, func, args[0].request.query, *args, **kwargs) 
        return wrapped
    return validate_get_params


def validate_json_params_decorator(**fields):
    """
    Validate params in an incomming request
    *fields = {
        'required': {<param_name1>: <regex1>, ...},
        'variable': {<param_name1>: <regex1>, ...},
    }
    """
    def validate_json_params(func):  
        async def wrapped(*args, **kwargs):
            try:
                params = await args[0].request.json()
            except:
                logger.exception(format_exc())
                params = {}
            finally:
                return await validate_request_params(fields, func,  params, *args, **kwargs) 
        return wrapped
    return validate_json_params


