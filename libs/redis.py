import asyncio
import asyncio_redis

from settings import REDIS


class RedisCon:
    def __init__(self, **con_params):
        self.con_params = con_params if con_params else REDIS

    async def __aenter__(self):
        self.con = await asyncio_redis.Pool.create(**self.con_params)
        return self

    async def __aexit__(self, *args, **kwargs):
        try:
            await self.con.close()
        except:
            pass

    async def execute(self, command, key, *args, **kwargs):
        """
        Execute Redis command.
        :command - command (function) name
        :key - Redis key
        :*args, **kwargs - function params
        """
        return await getattr(self.con, command)(key, *args, **kwargs)