import logging.config
from pprint import pprint
import secrets
import re
from passlib.hash import pbkdf2_sha256
import math

import aiohttp

from settings import *
from libs.redis import *
from libs.db import *


logging.config.dictConfig(LOG_CONFIG)
defaultLogger = logging.getLogger("main")


async def format_phone_number(number):
    number = re.sub('[^0-9]','', number)
    return f"7{number[1:]}"


async def make_url_query_string(**params):
    return '&'.join((f'{k}={v}' for k, v in params.items()))


async def send_request(method, url, logger=None, **request_params):
    """
    Send a request asynchronously
    :method = 'POST', 'GET', 'PUT', ...
    :url
    :request_params
    :logger - if None = defaultLogger
    """
    if logger is None:
        logger = defaultLogger
    logger.info(f'{method}-запрос на {url}')
    for param, values in request_params.items():
        logger.info(f'{param}: {str(values)}')

    async with aiohttp.request(method, url, **request_params) as response:
        try:
            ret = await response.json()
            logger.info(f'Ответ:\n{str(ret)[:2000]}')
        except:
            ret = response
            logger.info(f'Ответ:\n{(await ret.text())[:2000]}')
        finally:       
            return ret
        #async with session.post(f'{ACQUIRING_OUT_URL}register', json=params) as response:
            #respJson = await response.json()


async def generate_user_tokens(data, token_lifetime=TOKEN_LIFETIME, refresh_token_lifetime=REFRESH_TOKEN_LIFETIME):
    token = secrets.token_urlsafe(TOKEN_LEN)
    refreshToken = secrets.token_urlsafe(TOKEN_LEN)
    data = str(data)
    async with RedisCon() as redis:
        await redis.execute('set', f'{REDIS_TOKEN_PREFIX}{token}', data, expire=token_lifetime)
        await redis.execute('set', f'{REDIS_TOKEN_UPDATE_PREFIX}{token}', refreshToken, expire=refresh_token_lifetime)
        await redis.execute('set', f'{REDIS_ACCESS_REFRESH_TOKEN_PREFIX}{token}:{refreshToken}', data, expire=refresh_token_lifetime)
    return {'access_token': token, 'refresh_token': refreshToken, 'expires_in': token_lifetime}


async def delete_user_tokens(access_token, refresh_token):
    delList = [f'{REDIS_TOKEN_PREFIX}{access_token}', f'{REDIS_TOKEN_UPDATE_PREFIX}{access_token}']
    userId = None
    async with RedisCon() as redis:                   
        accessRefrTokenKey = f'{REDIS_ACCESS_REFRESH_TOKEN_PREFIX}{access_token}:{refresh_token}'
        userId = await redis.execute('get', accessRefrTokenKey)
        if userId is not None:
            delList.append(accessRefrTokenKey)
        await redis.execute('delete', delList)
    return userId


async def ddmmyyyy_iso(d):
    return f"{d[6:10]}-{d[3:5]}-{d[0:2]}"


async def serialize_db_list(db_list):
    return [dict(rec) for rec in db_list]


async def calc_new_latitude(lat0, dist):
    return lat0  + (dist / EARTH_RADUIS) * (180 / math.pi);


