from aiohttp import web

from settings import *
from libs.decorators import validate_json_params_decorator, get_user_by_token_decorator
from libs.db import *


class EditRequest(web.View):

    async def edit(self, data, where_fields, where_values, table):
        keys = data.keys()
        if not keys:    #nothing to update
            return {'code': OK_CODE}

        statement = f'UPDATE {table} SET '
        values = []
        iUp = None
        for (iUp, k) in enumerate(keys):
            statement = f'{statement}{k} = ${iUp + 1},'
            values.append(data[k])
        statement = f"{statement[:-1]} WHERE"
        for (i, f) in enumerate(where_fields):
            statement = f"{statement} {f} = ${i + 2 + iUp} AND"
        statement = statement[:-4]
        print(statement, values)
        async with DataBase() as db:
            dbResp = await db.execute_query(
                statement,
                *values, *where_values
            )
        return {'code': OK_CODE}


class UploadAvatar(web.View):

    async def upload(self, dst):
        async for field in (await self.request.multipart()):
            if field.name == 'img':
                with open(os.path.join(dst), 'wb') as fd:
                    while True:
                        chunk = await field.read_chunk()
                        if not chunk:
                            break
                        fd.write(chunk)
        return {'code': OK_CODE}