import asyncio
import asyncpg

from settings import DATABASES


class DataBase:

    def __init__(self, **con_params):
        self.con_params = con_params if con_params else DATABASES['default-asyncpg']

    async def __aenter__(self):
        self.con = await asyncpg.connect(**self.con_params)
        return self

    async def __aexit__(self, *args, **kwargs):
        try:
            await self.con.close()
        except:
            pass

    async def execute_query(self, query, *params):
        ret = await self.con.fetch(query, *params)
        return ret