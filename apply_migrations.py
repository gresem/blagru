import asyncio
import asyncpg

from libs.db import DataBase 
from migrations import migrations


async def get_migrations():
    async with DataBase() as db:
        return await db.execute_query("""SELECT * FROM migrations ORDER BY id DESC""")

async def migrate():    
        try:
            dbResp = await get_migrations()
        except asyncpg.exceptions.UndefinedTableError:
            async with DataBase() as db:
                print('Creating table "migrations"')
                await db.execute_query("""
                    CREATE TABLE public.migrations (
                        id smallint PRIMARY KEY,
                        time timestamptz DEFAULT now()
                    );
                    """)
            dbResp = await get_migrations()        
        startIndex = dbResp[0]['id'] + 1 if dbResp else 0
        async with DataBase() as db:
            for i, mig in enumerate(migrations[startIndex:]):
                migId = i + startIndex
                print(f'Applying migration {migId}...')
                await db.execute_query('BEGIN')
                for i, st in enumerate(mig):
                    print(i)
                    await db.execute_query(st)
                await db.execute_query('INSERT INTO migrations (id) VALUES ($1);', migId)
                await db.execute_query('COMMIT')
                print(f'Done with migration {migId}...')


if __name__ == '__main__':
    asyncio.run(migrate())