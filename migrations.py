import socket

migrations = [
    #0. create tables: user, event_type, events, user_event
    [
        """  
        CREATE TABLE public.user (
            id SERIAL PRIMARY KEY,
            password VARCHAR NOT NULL,
            last_login TIMESTAMP WITH TIME ZONE,          
            login VARCHAR UNIQUE NOT NULL,
            email VARCHAR UNIQUE NOT NULL,
            is_active BOOLEAN NOT NULL,
            date_joined TIMESTAMP WITH TIME ZONE NOT NULL,
            phone_number VARCHAR(20) UNIQUE NOT NULL
        ) 
        """,
        """
        CREATE INDEX user__login__index ON public.user
            USING btree (login);
        """,
        """
        CREATE INDEX user__phone_number__index ON public.user
            USING btree (phone_number);
        """,
        """
        CREATE TABLE public.event_type(
            id SERIAL PRIMARY KEY,
            name VARCHAR NOT NULL
        )
        """,
        """  
        CREATE TABLE public.event (
            id BIGSERIAL PRIMARY KEY,
            type INTEGER REFERENCES event_type NOT NULL,
            start_latitude NUMERIC(9),
            start_attitude NUMERIC(9),
            end_latitude NUMERIC(9) NOT NULL,
            end_attitude NUMERIC(9) NOT NULL,
            comment VARCHAR
        ) 
        """,
        """
        CREATE INDEX user__start_latitude__index ON public.event
            USING btree (start_latitude);
        """,
        """
        CREATE INDEX user__start_attitude__index ON public.event
            USING btree (start_attitude);
        """,
        """
        CREATE INDEX user__end_latitude__index ON public.event
            USING btree (end_latitude);
        """,
        """
        CREATE INDEX user__end_attitude__index ON public.event
            USING btree (end_attitude);
        """,
        '''INSERT INTO event_type (name) VALUES('Совместная поездка')''',
        '''
        CREATE TABLE public.user_event(
            id BIGSERIAL PRIMARY KEY,
            user_id INTEGER REFERENCES public.user NOT NULL,
            event_id BIGINT REFERENCES public.event NOT NULL
        )
        ''',        
    ],
    #1. table google_account
    [
        '''
        CREATE TABLE public.google_account(
            id BIGINT PRIMARY KEY,
            user_id INTEGER REFERENCES public.user NOT NULL
        )
        '''
    ],
    #2. add name to user table
    [
        '''ALTER TABLE public.user ADD COLUMN name VARCHAR'''
    ],
    #3. login allow null
    [
        '''ALTER TABLE public.user ALTER COLUMN login DROP NOT NULL'''
    ],
    #4. alter login table
    [
        'ALTER TABLE public.user ALTER COLUMN password DROP NOT NULL',
        'ALTER TABLE public.user ALTER COLUMN email DROP NOT NULL',
        'ALTER TABLE public.user ALTER COLUMN phone_number DROP NOT NULL',
        'ALTER TABLE public.user ALTER COLUMN date_joined SET DEFAULT NOW()',
        'ALTER TABLE public.user ALTER COLUMN is_active SET DEFAULT TRUE',
    ],
    #5. alter google_account table
    [
        'ALTER TABLE google_account ADD CONSTRAINT google_account__user_id__unique UNIQUE (user_id)'
    ],
    #6. table with client id/secret
    [
        '''
        CREATE TABLE public.api_client(
            id VARCHAR PRIMARY KEY,
            secret VARCHAR NOT NULL,
            name VARCHAR NOT NULL
        )
        ''',
        '''
        INSERT INTO api_client (id, secret, name) 
            VALUES('33699ed9546241c9b3a647e36a0cbf3f', '67e8f686fef24f9b9b86da64aaf2b289', 'IOS app')
        '''
    ],
    #7. alter google_account table
    [
        'ALTER TABLE google_account ALTER COLUMN id TYPE VARCHAR'
    ],
    #8. rename user table
    [
        'ALTER TABLE public.user RENAME TO auth_user'
    ],
    #9. make name in event_type unique
    [
        'ALTER TABLE public.event_type ADD CONSTRAINT event_type__name__unique UNIQUE (name)'
    ],
    #10. add column user_id to event table
    [
        'ALTER TABLE event ADD COLUMN user_id INTEGER REFERENCES auth_user NOT NULL'
    ],
    #11. rename column
    [
        'ALTER TABLE event RENAME COLUMN type TO type_id'
    ],
    #12. rename columns
    [
        'ALTER TABLE event RENAME COLUMN start_attitude TO lon0',
        'ALTER TABLE event RENAME COLUMN end_attitude TO lon1',
        'ALTER TABLE event RENAME COLUMN start_latitude TO lat0',
        'ALTER TABLE event RENAME COLUMN end_latitude TO lat1',
    ],
    #13. add precison
    [
        'ALTER TABLE event ALTER COLUMN lat0 TYPE NUMERIC(9,6)',
        'ALTER TABLE event ALTER COLUMN lat1 TYPE NUMERIC(9,6)',
        'ALTER TABLE event ALTER COLUMN lon0 TYPE NUMERIC(9,6)',
        'ALTER TABLE event ALTER COLUMN lon1 TYPE NUMERIC(9,6)',
    ],
    #14. add composite unique constraint
    [
        'ALTER TABLE event ADD UNIQUE (lon0, lat0, lon1, lat1, user_id)'
    ],
    #15. add postgis
    [
        'CREATE EXTENSION postgis',
        'CREATE EXTENSION postgis_sfcgal',
        'CREATE EXTENSION address_standardizer',
        'CREATE EXTENSION fuzzystrmatch',
        'CREATE EXTENSION postgis_topology',
        'CREATE EXTENSION postgis_tiger_geocoder',
    ],
    #16. fill coords column
    [
        '''ALTER TABLE public.event ADD COLUMN coords0 GEOGRAPHY''',
        '''ALTER TABLE public.event ADD COLUMN coords1 GEOGRAPHY''',
        'UPDATE public.event SET coords0 = ST_POINT(lat0, lon0) where lat0 IS NOT NUll',
        'UPDATE public.event SET coords1 = ST_POINT(lat1, lon1)',        
    ],
    #17. make coords1 not null
    [
        'ALTER TABLE public.event ALTER COLUMN coords1 SET NOT NULL;'
    ],
    #18. rename columns
    [
        'ALTER TABLE event RENAME COLUMN coords0 TO point0',
        'ALTER TABLE event RENAME COLUMN coords1 TO point1',
    ],
    #19. add composite unique constraint
    [
        'ALTER TABLE event ADD UNIQUE (point0, point1, user_id)'
    ],
    #20. add composite unique constraint
    [
        'ALTER TABLE event DROP COLUMN lat0, DROP COLUMN lat1, DROP COLUMN lon0, DROP COLUMN lon1;'
    ],
    #21. add index
    [
        'CREATE INDEX ON event USING GIST(point1)',
        'CREATE INDEX ON event USING GIST(point0)'
    ],
    #22. change index
    [
        'ALTER TABLE event DROP CONSTRAINT event_point0_point1_user_id_key CASCADE',
        'ALTER TABLE event ADD UNIQUE (point0, point1, user_id, type_id)'
    ],
    #23. drop index
    [
        'ALTER TABLE event DROP CONSTRAINT event_point0_point1_user_id_type_id_key CASCADE',
    ],
    #24. add time to event table
    [
        'ALTER TABLE event ADD COLUMN time TIMESTAMP DEFAULT NOW() NOT NULL',
    ],
    #25. favorite events
    [
        '''
        CREATE TABLE public.favorite_event(
            event_id BIGINT REFERENCES public.event NOT NULL,
            user_id INTEGER REFERENCES public.auth_user NOT NULL
        )
        ''',
        'ALTER TABLE public.favorite_event ADD UNIQUE (event_id, user_id)'
    ],
]