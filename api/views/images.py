from aiohttp import web

from settings import *
from libs.decorators import validate_json_params_decorator, get_user_by_token_decorator
from libs.templates import UploadAvatar


class EventTypeUploadAvatar(UploadAvatar):

    async def post(self):        
        return web.json_response(await self.upload(os.path.join(EVENTTYPE_AVATARS_DIR, f"{self.request.match_info['id']}.png")))


class UserUploadAvatar(UploadAvatar):
   
    @get_user_by_token_decorator
    async def post(self):        
        return web.json_response(await self.upload(os.path.join(USER_AVATARS_DIR, f"{self.request.user['id']}.jpg")))