import json
from datetime import datetime

from aiohttp import web
import asyncpg

if __name__ == '__main__':
    import os
    import sys
    import requests
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    r = requests.post(
        'http://localhost:8000/api/v1/user/login/', 
        json={'login': 'user32', 'password': 'qqqqqq'},
        headers={"client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )
    token = r.json()['access_token']
    '''r = requests.post(
        'http://localhost:8000/api/v1/events/create/', 
        json={'type_id': 1, 'lat1': 50.31, 'lon1': 200.84746}, 
        headers={"token": r.json()['access_token']}
    )'''
    '''f = open('D:\\Downloads\\IMG_20200509_164524.jpg', 'rb')
    r = requests.post(
        'http://localhost:8000/api/v1/event_types/1/upload_avatar/', 
        files={'img': f},
        headers={"client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )'''
    '''r = requests.post(
        'http://localhost:8000/api/v1/events/add_favorite/', 
        json={'event_id': 5}, 
        headers={"token": token, "client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )'''
    '''r = requests.post(
        'http://localhost:8000/api/v1/events/remove_favorite/', 
        json={'event_id': 1}, 
        headers={"token": r.json()['access_token'], "client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )'''
    '''r = requests.post(
        'http://localhost:8000/api/v1/events/get_favorite/', 
        headers={"token": token, "client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )'''
    '''r = requests.post(
        'http://localhost:8000/api/v1/events/edit/', 
        json={'id': 2, 'lat0': 55.683111, 'lon0': 37.475111}, 
        headers={"token": token, "client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )'''
    r = requests.post(
        'http://localhost:8000/api/v1/events/remove/', 
        json={'id': 2}, 
        headers={"token": token, "client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )
    print(r.text)

from settings import *
from libs.common import send_request, serialize_db_list, calc_new_latitude
from libs.db import *
from libs.redis import *
from libs.decorators import validate_json_params_decorator, get_user_by_token_decorator


class GetTypes(web.View):

    async def post(self):
        async with DataBase() as db:
            dbResp = await db.execute_query('SELECT * from event_type')
        return web.json_response({'code': OK_CODE, 'event_types': await serialize_db_list(dbResp)})


class CreateType(web.View):

    @get_user_by_token_decorator
    @validate_json_params_decorator(required={'name': r'^.+$'})
    async def post(self):
        #async with RedisCon() as redis:
        #    await redis.execute('delete', [f'{REDIS_TOKEN_PREFIX}{"EhtG3xkaNILWeJyR_yvssyWzZTDOO5VQ0WFo2wJoZWgOu_gHrYRDoo9C3cQil6_YrQnmidkriOBIGFxlJTkUqvEk6X7umf6fAO7SfkCnpQP0dssIqs-zl038Mxyh9REU8mUAIPLmrnuHJB_4KVCBohHDsj2PP3-ww7vDYSijjrQ"}'])
    
        return web.json_response(await self.create())

    async def create(self):
        data = await self.request.json()
        name = data['name'].strip()
        ret = {}
        async with DataBase() as db:
            try:
                dbResp = await db.execute_query('INSERT INTO event_type (name) VALUES($1) RETURNING id', name)
                ret = {'code': OK_CODE}
            except asyncpg.exceptions.UniqueViolationError:
                dbResp = await db.execute_query('SELECT id FROM event_type WHERE name = $1', name)
                ret = {'code': OPERATION_EXISTS}
        ret['id'] = dbResp[0]['id']
        return ret


class CreateEvent(web.View):

    @get_user_by_token_decorator
    @validate_json_params_decorator(
        required={'type_id': r'^\d+$', 'lat1': COORD_REGEX, 'lon1': COORD_REGEX},
        variable={'lat0': COORD_REGEX, 'lon0': COORD_REGEX, 'comment': r'.+', 'time': DATETIME_ISO_REGEX}
    )
    async def post(self):
        return web.json_response(await self.create())

    async def create(self):
        data = await self.request.json()
        ret = {}
        values = [data['type_id'], self.request.user['id'], data['lat1'], data['lon1']]
        if data.get('lat0') is not None and data.get('lon0') is not None:
           values.extend([data['lat0'], data['lon0']]) 
        async with DataBase() as db:
            dbResp = await db.execute_query(f'''
                SELECT COUNT(*) FROM event 
                WHERE type_id = $1 AND user_id = $2 AND point1 = ST_POINT($3, $4) 
                    {'AND point0 = ST_POINT($5, $6)' if data.get('lat0') is not None and data.get('lon0') is not None else ''}''',
                *values
            )
            if len(values) == 4:
                values.extend([None, None]) 
            if dbResp and dbResp[0]['count'] == 0:
                dbResp = await db.execute_query('''
                    INSERT INTO event (type_id, user_id, point1, point0, comment, time)
                        VALUES($1, $2, ST_POINT($3, $4), ST_POINT($5, $6), $7, $8) RETURNING id''',
                    *values, data.get('comment'), data.get('time', datetime.now())
                )
                ret = {'code': OK_CODE, 'id': dbResp[0]['id']}
            else:
                ret['code'] = OPERATION_EXISTS
        return ret


class RemoveEvent(web.View):

    @get_user_by_token_decorator
    @validate_json_params_decorator(required={'id': r'^\d+$'})
    async def post(self):
        return web.json_response(await self.remove())

    async def remove(self):
        data = await self.request.json()
        userId = self.request.user['id']
        async with DataBase() as db:
            await db.execute_query('DELETE FROM user_event WHERE user_id = $1 AND event_id = $2', userId, data['id'])
            dbResp = await db.execute_query('SELECT COUNT(*) FROM user_event WHERE user_id = $1 AND event_id = $2', userId, data['id'])
            if dbResp and dbResp[0]['count'] == 0:
                await db.execute_query('DELETE FROM event WHERE user_id = $1 AND id = $2', userId, data['id'])
        return {'code': OK_CODE}


class GetEvents(web.View):

    @validate_json_params_decorator(
        required={'type_id': r'^\d+$', 'lat1': COORD_REGEX, 'lon1': COORD_REGEX},
        variable={'lat0': COORD_REGEX, 'lon0': COORD_REGEX, 'radius': r'^\d{1,5}$', 'user_id': r'^\d+$'}
    )
    async def post(self):
        ret = json.dumps(await self.get_events(), default=str)
        return web.Response(body=ret)

    async def get_events(self):
        data = await self.request.json()
        async with DataBase() as db:
            dbResp = await db.execute_query('''
                SELECT ST_X(point0::geometry) as lat0, ST_Y(point0::geometry) as lon0, 
                    ST_X(point1::geometry) as lat1, ST_Y(point1::geometry) as lon1,
                    e.comment as comment, u.id as user_id, u.name as user_name, e.id as id, e.time as time
                FROM event e
                INNER JOIN auth_user u ON e.user_id = u.id
                WHERE u.id <> $1 AND e.type_id = $2 AND
                    ST_DWithin(point1, ST_MakePoint($3, $4)::geography, $5) OR
                    (point0 IS NOT NULL AND ST_DWithin(point0, ST_MakePoint($6, $7)::geography, $8))''',
                data.get('user_id', 0), data['type_id'], data['lat1'], data['lon1'], DEFAULT_RADIUS,
                data.get('lat0'), data.get('lon0'), DEFAULT_RADIUS
            )
        return {'code': OK_CODE, 'events': await serialize_db_list(dbResp)}



class AddFavoriteEvent(web.View):

    @get_user_by_token_decorator
    @validate_json_params_decorator(
        required={'event_id': r'^\d+$'},
    )
    async def post(self): 
        return web.json_response(await self.add())

    async def add(self):
        data = await self.request.json()
        try:
            async with DataBase() as db:
                await db.execute_query(
                    'INSERT INTO favorite_event (event_id, user_id) VALUES($1, $2);',
                    data['event_id'], self.request.user['id']
                )
        except asyncpg.exceptions.ForeignKeyViolationError:
            return {'code': NOT_FOUND}
        except asyncpg.exceptions.UniqueViolationError:
            pass
        return {'code': OK_CODE}


class RemoveFavoriteEvent(web.View):

    @get_user_by_token_decorator
    @validate_json_params_decorator(
        required={'event_id': r'^\d+$'},
    )
    async def post(self):
        data = await self.request.json()
        async with DataBase() as db:
            await db.execute_query(
                'DELETE FROM favorite_event WHERE event_id = $1 AND user_id = $2',
                data['event_id'], self.request.user['id']
            )
        return web.json_response({'code': OK_CODE})


class GetFavoriteEvents(web.View):

    @get_user_by_token_decorator
    async def post(self):
        data = await self.request.json()
        async with DataBase() as db:
            dbResp = await db.execute_query('''
                SELECT ST_X(point0::geometry) as lat0, ST_Y(point0::geometry) as lon0, 
                    ST_X(point1::geometry) as lat1, ST_Y(point1::geometry) as lon1,
                    e.comment as comment, e.id as id
                FROM event e
                INNER JOIN favorite_event fu ON e.id = fu.event_id
                WHERE fu.user_id = $1''',
                self.request.user['id']
            )
        return web.json_response({'code': OK_CODE, 'events': await serialize_db_list(dbResp)})


class EditEvent(web.View):

    @get_user_by_token_decorator
    @validate_json_params_decorator(
        required={'id': r'^\d+$'},
        variable={'type_id': r'^\d+$', 'lat1': COORD_REGEX, 'lon1': COORD_REGEX, 'lat0': COORD_REGEX, 'lon0': COORD_REGEX, 
                  'comment': r'.+', 'time': DATETIME_ISO_REGEX}
    )
    async def post(self):        
        return web.json_response(await self.edit())

    async def edit(self):
        data = await self.request.json()
        keys = data.keys() 
        if not keys:    #nothing to update
            return {'code': OK_CODE}   
        keys = [k for k in keys if k not in ['lat0', 'lon0', 'lat1', 'lon0']]
        statement = f'UPDATE event SET '
        values = []
        iUp = None if keys else -1
        for (iUp, k) in enumerate(keys):
            statement = f'{statement}{k} = ${iUp + 3},'
            values.append(data[k])
        if 'lat0' in data and 'lon0' in data:
            statement = f'{statement}point0 = ST_POINT(${iUp + 4}, ${iUp + 5}),'
            values.extend([data['lat0'], data['lon0']])
            iUp += 2
        if 'lat1' in data and 'lon1' in data:
            statement = f'{statement}point1 = ST_POINT(${iUp + 4}, ${iUp + 5}),'
            values.extend([data['lat1'], data['lon1']])
        statement = statement[:-1]
        async with DataBase() as db:
            dbResp = await db.execute_query(
                f'{statement} WHERE id = $1 and user_id = $2',
                data['id'], self.request.user['id'], *values,
            )
        return {'code': OK_CODE} 
        