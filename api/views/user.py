from aiohttp import web, web_exceptions
from urllib.parse import urlencode
from passlib.hash import pbkdf2_sha256

if __name__ == '__main__':
    import os
    import sys
    import requests
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    r = requests.post(
        'http://localhost:8000/api/v1/user/login/', 
        json={'login': 'user32', 'password': 'qqqqqq'},
        headers={"client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289'}
    )
    token = r.json()['access_token']
    '''r = requests.post('http://localhost:8000/api/v1/user/social_auth/google/', json={
        'token': 'ya29.a0AfH6SMDcct_7AZXBv8ae9ImTIWTpX9cC_jaz_4ywyNDXBCprNsmqDAlb0psTeCPFKRVOGtSSUSyScutDI6zSbtCpwf5JIMGpjXuYYAjimmDVvVJevPv2oAs5wfdV2qb6Kvk6SvZ8V1LuwLtYufQYz6mLcpsLLY4Yhao',
        'client_id': '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289',
        })'''
    '''r = requests.post('http://localhost:8000/api/v1/user/refresh_token/', json={
        'access_token': 'taopAv-9cvwVxuw1QUupmSNMS7aO6fHxWUxLU5PNn-9jis9H0gtmUvSt6MYYZLCuBv62I-UkKYNH1ekoyBA4YxBxazujEUcEXT8z_w0yMyBhPz9y9MKBVAPa0VvtU3TvTbnBooW0U_yB4qjeX9D8lw_RlUuBd9mYhdBnEfTcGpI',
        'refresh_token': 'Tx0R92NKtEkv6-OGu7HwjpgfMoOlSLif62__7e2B15-bYL8eSMoJHpgqYhztBGU0hAvVpA9QUnVbCoST_S-bXpbjCukHsXKIx3EGLgygGpZEEWEjAFpto19l1kVuRbUIJUfaZy8BQwmZNz2OVUQo3RQC6yRoOWYnhcTk1rz7GxE'
        })'''
    '''r = requests.post(
        'http://localhost:8000/api/v1/user/logout/',
        headers={"token": "tUBK5B2VEaMk69J1blXnwwrYL8eBt_Uq_o4sQhRyVSDPVRs-A7w3CuZP0T7Qka-Of95rLxt2BEwkLOhEpLxMnmKYLV3FHFax-wYHcxPFO18xjdl4rqKffps2CiwLWQtI6lFh1pdTzNyZW0oECGuWY1yB5CD7SmWKxL-pu9oOM-A"}
        )'''
    '''r = requests.post('http://localhost:8000/api/v1/user/register/', json={
        'login': 'qwew',
        'password': 'qqqqqq'
        })'''
    '''r = requests.post('http://localhost:8000/api/v1/user/login/', json={
        'login': 'qwew',
        'password': 'qqqqqq'
        })'''
    f = open('C:\\Users\\Grechishnikov-SR.NKO.LOCAL\\Downloads\\s1200.png', 'rb')
    r = requests.post(
        'http://localhost:8000/api/v1/user/upload_avatar/', 
        files={'img': f},
        headers={"client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289', 'token': token}
    )
    r = requests.post(
        'http://localhost:8000/api/v1/user/edit_info/',
        json={'name': 'xeq', 'phone_number': '+55344412344'},
        headers={"client_id": '33699ed9546241c9b3a647e36a0cbf3f', 'client_secret': '67e8f686fef24f9b9b86da64aaf2b289', 'token': token}
    )
    print(r.text)

from settings import *
from libs.common import send_request, generate_user_tokens, delete_user_tokens
from libs.db import *
from libs.redis import *
from libs.decorators import validate_json_params_decorator, get_user_by_token_decorator, get_user_by_logopass_decorator
from libs.templates import EditRequest


class GoogleAuth(web.View):

    @validate_json_params_decorator(required={'token': r'^.+$'})
    async def post(self):
        data = await self.request.json()
        guser = await send_request('GET', 'https://www.googleapis.com/oauth2/v2/userinfo', headers={'Authorization': f"Bearer {data['token']}"})
        return web.json_response(await self.get_user_info(guser))

    async def get_user_info(self, user_data):
        ret = {}
        async with DataBase() as db:
            dbResp = await db.execute_query('''
                SELECT u.id, u.name, u.login
                    FROM auth_user u
                    INNER JOIN google_account ga ON ga.user_id = u.id
                WHERE ga.id = $1''',
                user_data['id']
            )            
            userName = user_data.get('name')       
            ret['user'] = {'name': userName}
            if not dbResp:  #user not exist
                dbRespCr = await db.execute_query(
                    '''INSERT INTO auth_user (email, phone_number, name) VALUES($1, $2, $3) RETURNING id''',
                    user_data.get('email'), user_data.get('phone_number'), userName
                )
                userId = dbRespCr[0]['id']
                await db.execute_query(
                    'INSERT INTO google_account (id, user_id) VALUES($1, $2)',
                    user_data['id'], userId
                )
                ret['user']['id'] = userId
            else:
                ret['user']['id'] = dbResp[0]['id']
                if dbResp[0]['login'] is not None:
                    ret['user']['login'] = dbResp[0]['login']
            ret['code'] = OK_CODE
        ret.update(await generate_user_tokens(ret['user']['id']))        
        return ret


class Login(web.View):

    @validate_json_params_decorator(required={'login': r'^.+$', 'password': r'^.+$'})
    @get_user_by_logopass_decorator
    async def post(self):
        ret = {'code': OK_CODE}
        ret.update(await generate_user_tokens(self.request.user['id']))
        ret['user'] = {'id': self.request.user['id'], 'login': self.request.user['login']}
        return web.json_response(ret)


class RefreshToken(web.View):

    @validate_json_params_decorator(required={'access_token': r'^.+$', 'refresh_token': r'^.+$'})
    async def post(self):
        return web.json_response(await self.refresh())

    async def refresh(self):
        data = await self.request.json()
        userId = await delete_user_tokens(data['access_token'], data['refresh_token'])
        if userId is None:
            raise web_exceptions.HTTPForbidden
        ret = await generate_user_tokens(userId)
        async with DataBase() as db:
            dbResp = await db.execute_query('''SELECT id, name, login FROM auth_user WHERE id = $1''', int(userId)) 
        ret['user'] = dict(dbResp[0])
        return ret


class Logout(web.View):

    @get_user_by_token_decorator
    async def post(self):
        token = self.request.user['token']
        async with RedisCon() as redis: 
            refreshToken = await redis.execute('get', f'{REDIS_TOKEN_UPDATE_PREFIX}{token}')
        await delete_user_tokens(token, refreshToken)
        return web.json_response({'code': OK_CODE})


class Register(web.View):

    async def check_unique(self, param, value, db):
        dbResp = await db.execute_query(f'SELECT count(*) FROM auth_user WHERE {param} = $1', value)
        return {'code': OPERATION_EXISTS, 'errors': {param: 'User with this value exists'}} if dbResp[0]['count'] != 0 else True
    
    @validate_json_params_decorator(
        required={'login': LOGIN_REGEX, 'password': USER_PASSWORD_REGEX},
        variable={'name': r'\w+', 'email': EMAIL_REGEX, 'pnone_number': PHONE_NUMBER_REGEX}
    )
    async def post(self):
        return web.json_response(await self.create_user())

    async def create_user(self):
        data = await self.request.json()
        h = pbkdf2_sha256.hash(data['password'])
        name = data.get('name')
        async with DataBase() as db:   
            for param in ['login', 'email', 'phone_number']:
                if param in data:
                    ret = await self.check_unique(param, data[param], db)
                    if ret != True:
                        return ret
            dbResp = await db.execute_query('''
                INSERT INTO auth_user
                    (login, password, name, email, phone_number)
                    VALUES($1, $2, $3, $4, $5)
                RETURNING id''',
                data['login'],  h, name, data.get('email'), data.get('phone_number')
            )
        ret = await generate_user_tokens(dbResp[0]['id'])
        ret.update({'code': OK_CODE, 'id': dbResp[0]['id']})
        ret['user'] = {'id': dbResp[0]['id'], 'login': data['login'], 'name': name}
        return ret


class GetUserInfo(web.View):

    @get_user_by_token_decorator
    @validate_json_params_decorator(
        variable={'id': r'^\d+$', 'login': LOGIN_REGEX,},
        or_params=(['id', 'login'],)
    )
    async def post(self):
        return web.json_response(await self.get_info())

    async def get_info(self):
        data = await self.request.json()
        key = 'id' if 'id' in data else 'login'
        async with DataBase() as db:
            dbResp = await db.execute_query(
                f'SELECT id, login, email, phone_number, name FROM auth_user WHERE {key} = $1',
                data[key]
            )
        if dbResp:
            return {'code': OK_CODE, 'user': dict(dbResp[0])}
        else:
            return {'code': NOT_FOUND}


class EditUserInfo(EditRequest):

    @get_user_by_token_decorator
    @validate_json_params_decorator(
        variable={'password': USER_PASSWORD_REGEX, 'login': LOGIN_REGEX, 'email': EMAIL_REGEX, 'phone_number': PHONE_NUMBER_REGEX, 'name': r'\w+'}
    )
    async def post(self):
        data = await self.request.json()
        if 'password' in data:
            data['password'] = pbkdf2_sha256.hash(data['password'])
        return web.json_response(await self.edit(data, ['id'], [self.request.user['id']], 'auth_user')) 
              


#for dev===============================================
class GetGoogleAuthUrl(web.View):    
    async def get(self):
        opts = {
            'scope': 'email profile',
            'state': 'security_token',
            'redirect_uri': 'http://localhost:8000/api/v1/user/google_auth_callback', # you need to register this with google
            'response_type': 'code',
            'client_id': GOOGLE_CLIENT_ID,
            'access_type': 'offline',  # Allows for refresh token
        }
        url = 'https://accounts.google.com/o/oauth2/v2/auth?' + urlencode(opts)
        return web.json_response({'code': OK_CODE, 'redirect_url': url})


class GoogleAuthCallback(web.View):
    async def get(self):
        data = self.request.query
        #if self.request.GET.get('error', None):
        #    raise web.HTTPUnauthorized(body=b'Unauthorized, denied Google Authentication')
        opts = {
            'code': data.get('code'),
            'client_id': GOOGLE_CLIENT_ID,
            'client_secret': GOOGLE_CLIENT_SECRET,
            'redirect_uri': 'http://localhost:8000/api/v1/user/google_auth_callback',
            'grant_type': 'authorization_code',
        }
        tokenInfo = await send_request('POST', 'https://www.googleapis.com/oauth2/v4/token', data=opts)
        print(tokenInfo)
        return web.json_response(tokenInfo)
        guser = await send_request('GET', 'https://www.googleapis.com/oauth2/v2/userinfo', headers={'Authorization': 'Bearer ' + tokenInfo['access_token']})
        return web.json_response(await self.get_user_info(guser))

