import os

from aiohttp import web
import aiohttp_jinja2


class Index(web.View):
    @aiohttp_jinja2.template(os.path.join('empty_page.html'))
    async def get(self): 
        return {'message': 'Эта страница не несёт в себе никакого смысла...'}