import os
import sys

APP_DIR = os.path.dirname(__file__)

sys.path.append(os.path.dirname(APP_DIR))

from main_settings.root import *