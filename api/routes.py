import os
import sys

sys.path.insert(0, os.path.dirname(__file__))

from main_routes import *
from views.index import Index
from views.user import GetGoogleAuthUrl, GoogleAuthCallback, GoogleAuth, RefreshToken, Logout, Register, Login, GetUserInfo, \
                       EditUserInfo
from views.events import GetTypes, CreateType, CreateEvent, GetEvents, AddFavoriteEvent, \
                        RemoveFavoriteEvent, GetFavoriteEvents, EditEvent, RemoveEvent
from views.images import EventTypeUploadAvatar, UserUploadAvatar
from settings import *

API1 = '/api/v1/'
USER1 = f'{API1}user/'
EVENT_TYPES1 = f'{API1}event_types/'
EVENTS1 = f'{API1}events/'


def setup_routes(app):    
    app.router.add_get('/', Index)     
    app.router.add_post(f'{USER1}social_auth/google/', GoogleAuth) 
    app.router.add_post(f'{USER1}refresh_token/', RefreshToken) 
    app.router.add_post(f'{USER1}logout/', Logout) 
    app.router.add_post(f'{USER1}register/', Register) 
    app.router.add_post(f'{USER1}login/', Login) 
    app.router.add_post(f'{USER1}get_info/', GetUserInfo)
    app.router.add_post(f'{USER1}upload_avatar/', UserUploadAvatar)
    app.router.add_post(f'{USER1}edit_info/', EditUserInfo)

    app.router.add_post(f'{EVENT_TYPES1}get/', GetTypes) 
    app.router.add_post(f'{EVENT_TYPES1}create/', CreateType)
    app.router.add_post(f'{EVENT_TYPES1}{{id}}/upload_avatar/', EventTypeUploadAvatar)

    app.router.add_post(f'{EVENTS1}create/', CreateEvent)
    app.router.add_post(f'{EVENTS1}get/', GetEvents)
    app.router.add_post(f'{EVENTS1}add_favorite/', AddFavoriteEvent)
    app.router.add_post(f'{EVENTS1}remove_favorite/', RemoveFavoriteEvent)
    app.router.add_post(f'{EVENTS1}get_favorite/', GetFavoriteEvents)
    app.router.add_post(f'{EVENTS1}edit/', EditEvent)
    app.router.add_post(f'{EVENTS1}remove/', RemoveEvent)

    if not IS_PROD:
        app.router.add_get(f'{USER1}google_auth_url', GetGoogleAuthUrl)   
        app.router.add_get(f'{USER1}google_auth_callback', GoogleAuthCallback)  
    setup_static_routes(app)
