from settings import *


def setup_static_routes(app):
    app.router.add_static(STATIC_URL, path=STATIC_DIR, name='static')
    app.router.add_static(MEDIA_URL, path=MEDIA_DIR, name='media')
