from base64 import b64encode
from traceback import format_exc
import logging.config

from aiohttp import web, web_exceptions
from passlib.hash import pbkdf2_sha256

from settings import *
from libs.db import *
from libs.redis import *


logging.config.dictConfig(LOG_CONFIG)
logger = logging.getLogger("main")


@web.middleware
async def exceptions_handling_middleware(request, handler):
    try:
        response = await handler(request)        
        return response
    except web_exceptions.HTTPNotFound:
        raise web_exceptions.HTTPNotFound
    except web_exceptions.HTTPMethodNotAllowed:
        return web.HTTPMethodNotAllowed()
    except web_exceptions.HTTPForbidden:
        return web.HTTPForbidden()
    except Exception as e:
        logger.exception(format_exc())
        return web.json_response({'code': ERROR_CODE})


@web.middleware
async def request_response_logging(request, handler):
    logger.info(f"Входящий запрос: {request.url}")
    logger.info(f"Query params: {request.query}")
    try:
        data = await request.json()
        if 'password' in data:
            data['password'] = '***'
        logger.info(f"JSON: {data}")
    except:
        pass
    response = await handler(request)
    logger.info(f'Ответ на запрос: {response._body}')
    return response


@web.middleware
async def get_user_middleware(request, handler):
    data = await request.json()
    dbResp = None
    authenticated = False
    if 'login' in data and 'password' in data:    #auth by username and pasword
        async with DataBase() as db:
            dbResp = await db.execute_query("""
                SELECT id, password, is_active, username, organisation_id
                    FROM auth_user au 
                    INNER JOIN user_info ui ON (au.id = ui.user_id)
                    WHERE username = $1
                """, 
                data['login']
            )
        if dbResp: #user found in DB
            dbResp = dbResp[0]
            if dbResp['is_active']:
                password = dbResp['password'].split('$')
                password[0] = password[0].replace('_', '-')
                password[2] = b64encode(password[2].encode('utf8')).decode('utf8')
                if pbkdf2_sha256.verify(data['password'], f'${"$".join(password)}'):
                    authenticated = True
    if authenticated:    #user exists
        request.user = {'id': dbResp['id'], 'login': dbResp['username'], 'organisation_id': dbResp['organisation_id']}
    else:   #error
        return web.HTTPForbidden() #web.json_response({'code': ERROR_CODE, 'description': 'Incorrect authentication data'})
    resp = await handler(request)    
    return resp


@web.middleware
async def  check_client_middleware(request, handler):
    data = request.headers
    if not IS_PROD and ('/static/' in str(request.url) or '/media/' in str(request.url)):
        return await handler(request)
    if 'client_id' in data and 'client_secret' in data:
        async with DataBase() as db:
            dbResp = await db.execute_query("""
                SELECT COUNT(*)
                FROM api_client 
                WHERE id = $1 AND secret = $2
                """, 
                data['client_id'], data['client_secret']
            )
        if dbResp and dbResp[0]['count'] == 1:
            return await handler(request)
    return web.HTTPForbidden()
