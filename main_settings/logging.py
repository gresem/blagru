import os
import sys
from traceback import format_exc


LOG_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'logs')


if not os.path.exists(LOG_PATH):
    os.makedirs(LOG_PATH)

LOG_CONFIG = {
    'version': 1,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s] [%(levelname)s] [%(filename)s::%(funcName)s::%(lineno)d]- %(message)s'}
    },
    'handlers': {        
        'main': {            
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'main.log'),
            'formatter': 'standard',            
            'level': 'INFO',
        },   
        'error': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_PATH, 'errors.log'),
            'formatter': 'standard'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {  
        'main': {
            'handlers': ['main', 'error', 'console'],            
            'propagate': True,
            'level': 'INFO',
        },
    },
}
