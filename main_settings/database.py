from .root import *


if IS_PROD:
    pass
else:
    DATABASES = {
        'default': {
            'HOST': 'localhost',
            'PORT': '5432',
            'NAME': 'taxsharing',
            'USER': 'postgres',
            'PASSWORD': 'Password1',
        },
    }

    REDIS = {
        'host': 'localhost',
        'port': 6379,
        'poolsize': 10,
    }

DATABASES['default-asyncpg'] = {
    'host': DATABASES['default']['HOST'],
    'port': DATABASES['default']['PORT'],
    'database': DATABASES['default']['NAME'],
    'user': DATABASES['default']['USER'],
    'password': DATABASES['default']['PASSWORD'],
}