import os
from .root import *

STATIC_URL = '/static/'
STATIC_DIR = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_DIR = os.path.join(BASE_DIR, 'media')

EVENTTYPE_AVATARS_DIR = os.path.join(MEDIA_DIR, 'event_type_avatars')
USER_AVATARS_DIR = os.path.join(MEDIA_DIR, 'user_avatars')


for d in [STATIC_DIR, MEDIA_DIR, EVENTTYPE_AVATARS_DIR, USER_AVATARS_DIR]:
    if not os.path.exists(d):
        os.makedirs(d)