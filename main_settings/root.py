import os
import socket


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

HOSTNAME = socket.gethostname()

IS_PROD = False

from main_settings.database import *
from main_settings.logging import *
from main_settings.pathes import *
from main_settings.codes import *
from main_settings.regex_patterns import *
from main_settings.external_systems import *
from main_settings.api import *
from main_settings.values import *